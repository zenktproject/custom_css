/*
 * custom sidebartoggle 
 */

/**
 * Some events have been deprecated in 7.2 and removed.
 * List of changes:
 *
 * - `sidebarRendered` has been removed. Now, when the field renders it calls
 *    {@link Layout.Default#isSidePaneVisible} directly to get the current
 *    state.
 *
 * - `toggleSidebar` has been removed. Triggers `sidebar:toggle` instead.
 *
 * - `toggleSidebarArrows` has been removed. Listens to `sidebar:state:changed`
 *    instead.
 *
 * - `openSidebarArrows` has been removed. Listens to `sidebar:state:changed`
 *    instead.
 *
 * - The app event `app:toggle:sidebar` has been removed. Listen to
 *   `sidebar:state:changed` instead.
 */
({
    events: {
        'click': 'toggle'
    },

    /**
     * Store the current `open` or `close` state
     *
     * @type {String}
     */
    _state: 'close',

    /**
     * @inheritDoc
     */
    initialize: function(options) {
        this._super('initialize', [options]);

        var defaultLayout = this.closestComponent('sidebar');
		var toggle_flg = defaultLayout.isSidePaneVisible();
		if(toggle_flg === true){
			this.toggle(this.event);		//折り畳み
		}
        if (defaultLayout && _.isFunction(defaultLayout.isSidePaneVisible)) {
            this.toggleState(defaultLayout.isSidePaneVisible() ? 'open' : 'close');
            this.listenTo(defaultLayout, 'sidebar:state:changed', this.toggleState);
        }
    },

    /**
     * Toggle the `open` or `close` class of the icon.
     *
     * @param {String} [state] The state. Possible values: `open` or `close`.
     */
    toggleState: function(state) {
        if (state !== 'open' && state !== 'close') {
            state = (this._state === 'open') ? 'close' : 'open';
        }
        this._state = state;

        if (!this.disposed) {
            this.render();
        }
    },

    /**
     * Toggle the sidebar.
     *
     * @param {Event} event The `click` event.
     */
    toggle: function(event) {
        var defaultLayout = this.closestComponent('sidebar');
        if (defaultLayout) {
            defaultLayout.trigger('sidebar:toggle');
        }
    }
})
