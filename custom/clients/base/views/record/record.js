/*
 * custom - record.js  /custom/clients/base/views/
 */
({
	extendsFrom: 'RecordView',

	initialize: function(options) {
		this._super('initialize', [options]);
		
		//グリッドCSS 追加
		this.setCustomCss();
		
	},
	
	/**
	 * 現在のモジュールのrecordViewに対応するレイアウト変更CSSを適用
	 */
	setCustomCss: function(){
		var self = this;
		//var module      = self.model.module, // 現在のモジュール名取得-20140924追記
		//    cstmCssName = 'customCss-'+module; // 現在のモジュールに対応するCSSを適用するための、ID名を作成-20140924追記
		var cstmCssName = 'customCss-all'; // 全てのモジュールに適用する場合は、上記2行の代わりにこの行を使用
		//$('div.main-pane div.record').addClass(cstmCssName); // ← 何をしてたのか忘れました-20140924追記
		
		// カレントモジュール用のstyleタグを持たない場合、作成してheadへ
		if($('style#'+cstmCssName).length < 1){
			$('head').append('<style id="'+cstmCssName+'"></style>');
		}
		
		// カレントモジュール用のレイアウト変更CSSを対応するstyleタグへ入力
		// if文使って上の module の値(Accounts, Cases等)毎にここの記述を変えれば良さそうな感じです-20140924追記
		// グリッド縦線記述退避   + '.record.'+cstmCssName+' .row-fluid>div:not(:first-child){border-left: 1px dashed #ccc} '
		
		var cstmCssStr = '.record.'+cstmCssName+' div.row-fluid:not(:first-child){border-top: 1px dashed #ccc} '
					   + '.record.'+cstmCssName+' .row-fluid>div:not(:first-child){dashed #ccc} '
					   + '.record.'+cstmCssName+' .record-label{padding-top: 2px;} '
					   + '.record.'+cstmCssName+' .record-label>span{padding-right:.5em; padding-left:.5em; border-radius:2px; font-size: 9pt; background-color: #f5f5f5} '
					   + 'div.record.'+cstmCssName+' div.row-fluid{padding-bottom:0px;} '
					   //+ '/*div.record.'+cstmCssName+' div.record-cell{margin-bottom:0px;} */'
					   + '.record.'+cstmCssName+' div.row-fluid>div.record-cell>span>span.edit input{font-size:14px;} '
					   + '.record.'+cstmCssName+' div.row-fluid>div.record-cell>span>span.edit textarea{font-size: 14px; height: 4em;} '
					   + 'div.row-fluid>div.record-cell>span>span.edit>div{font-size:14px;}';
		$('style#'+cstmCssName).text(cstmCssStr);
		
		// 同期が完了して、なおかつ.recordに
		// カレントモジュール用のレイアウト変更CSSが適用されるclass名を持たない場合、
		// カレントモジュール用のレイアウト変更CSSが適用されるclass名を追加する。
		// + ラベル名が空の物は非表示 + ラベル表示を変更する
		self.model.on('sync', function(){
			if($('div.record.'+cstmCssName).length < 1){
				$('div.record div.record-label').each(function(){
					if($(this).data('name') == ''){
						$(this).hide();
					}else{
						var lbl = $(this).text();
						$(this).html('<span>'+lbl+'</span>');
					}
				});
				$('div.main-pane div.record').addClass(cstmCssName);
			}
		}, self);
	},
	
})
